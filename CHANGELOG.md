# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-11-06

### Added

- A C# Acess Token emission example. 

[1.0.0]:https://gitlab.com/brytecnologia-team/integracao/api-autenticacao/csharp/emissao-e-gestao-jwt/tags/1.0.0
