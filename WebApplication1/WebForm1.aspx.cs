using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.IdentityModel.Tokens.Jwt;
using FluentScheduler;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        //client_id e client_secret da aplicação podem ser gerados em https://api-assinatura.bry.com.br/aplicacoes

        static string client_id = ""; //"client_id";
        static string client_secret = ""; //"client_secret";

        static string tokenAtual = ""; // Esta variável é populada com o access_token da resposta
        static string tokenRenovacao = ""; // Esta variável é populada com o refresh_token da resposta

        static int checkInterval = 60; // Intervalo entre checagens da duração do token, em segundos


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Inicializar_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (client_id == "" || client_secret == "")
                    throw new Exception("⚠ Please Insert a valid client_id and client_secret");

                // Chamando a função para inicializar a aplicação emitindo o primeiro par de tokens
                string responseInitialize = Inicializar().Result;

                RespostaToken responseInitializeObj = new RespostaToken();

                //Desserializando a resposta em um objeto proprio
                responseInitializeObj = Deserialize<RespostaToken>(responseInitialize);

                //Se ocorrer uma exceção na desserialização, saberemos aqui
                if (responseInitializeObj.access_token == null)
                    throw new Exception(responseInitialize + "\\n⚠ Please, check the submitted credentials.");

                else
                    // Popular a caixa de texto com os dados do token emitido e o seu respectivo token de renovação
                    this.TextBoxSaidaInicializar.Text = "Sucesso na emissão dos tokens. Para visualizar o token, clique no botão Visualizar.";
                    tokenAtual = responseInitializeObj.access_token;
                    tokenRenovacao = responseInitializeObj.refresh_token;
                    Console.WriteLine("Token de Acesso: " + tokenAtual);

                    if (tokenAtual == "" || tokenRenovacao == "")
                        throw new Exception("⚠ Please generate the initial access and refresh tokens!");

                    JobManager.Initialize(new RegistroAgendamento()); // inicializa o agendador de verificação da expiração


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }

        protected void Visualizar_Click(object sender, EventArgs e)
        {
            try
            {

                this.TextBoxSaidaInicializar.Text = "Token Atual: " + tokenAtual;


            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }



        public async static Task<string> Inicializar()
        {
            MultipartFormDataContent requestDataObj = new MultipartFormDataContent();

            requestDataObj.Add(new StringContent(client_id), "client_id");
            requestDataObj.Add(new StringContent(client_secret), "client_secret");
            requestDataObj.Add(new StringContent("client_credentials"), "grant_type");

            HttpClient client = new HttpClient();
 
            //Envia os dados para emissão do token JWT inicial a partir do client-id e client-secret
            var response = await client.PostAsync("https://cloud.bry.com.br/token-service/jwt", requestDataObj).ConfigureAwait(false);

            // O resultado da request é um json com o token de acesso, o token de renovação, e algumas infos
            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;

        }

        public async static Task<string> RenovaToken()
        {
            MultipartFormDataContent requestDataObj = new MultipartFormDataContent();
            requestDataObj.Add(new StringContent(tokenRenovacao), "refresh_token");
            requestDataObj.Add(new StringContent("refresh_token"), "grant_type");

            HttpClient client = new HttpClient();

            //Envia os dados para emissão do token de acesso utilizando o token de renovação
            var response = await client.PostAsync("https://cloud.bry.com.br/token-service/jwt", requestDataObj).ConfigureAwait(false);

            // O resultado da request é um json com o token de acesso, o token de renovação, e algumas infos
            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;

        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

        public class RegistroAgendamento : Registry
        {
            public RegistroAgendamento() // é o nosso agendador que ficará sempre checando a validade do token 
                                         // e emite um novo quando precisar
            {
                Schedule<CheckRenova>()
                .NonReentrant()  // Permite somente uma instância do job por vez
                .ToRunOnceAt(DateTime.Now.AddSeconds(2))  // Aguarda 2 segundos na primeira vez que rodar
                .AndEvery(checkInterval).Seconds(); // executa a checagem da validade do token para renovação
                                                    // toda vez que se passa o tempo em segundos definido em checkInterval
            }

        }

        public class CheckRenova : IJob
        {
            public void Execute()
            {
                JwtSecurityToken token = new JwtSecurityToken(tokenRenovacao);

                DateTimeOffset expiracao = token.ValidTo;
                long timestampExp = expiracao.ToUnixTimeMilliseconds();

                DateTimeOffset horaAtualUTC = DateTimeOffset.UtcNow;
                long timestampUTC = horaAtualUTC.ToUnixTimeMilliseconds();

                string renovado = "";

                if (timestampExp <= timestampUTC + checkInterval) // compara a hora atual UTC com o timestamp de expiração do token de renovação
                {
                    renovado = RenovaToken().Result; //irá renovar o par de tokens utilizando o tokenRenovacao
                    RespostaToken responseRenova = Deserialize<RespostaToken>(renovado);
                    tokenAtual = responseRenova.access_token; // atualiza os tokens com os novos emitidos agora
                    tokenRenovacao = responseRenova.refresh_token;

                    Console.WriteLine("Novo Token: " + tokenAtual); //mostra o novo token de acesso no console
                }
                else
                {
                    long time = (timestampExp - timestampUTC) / 60000; // tempo em minutos, que o token atual irá durar ainda
                    Console.WriteLine("Tempo remanescente para utilização do token atual: " + time + " minutos.");
                    
                }
            }

            public CheckRenova()
            {

            }
        }

    }

    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    
    public class RespostaToken
    {
        [DataMember]
        public string access_token;

        [DataMember]
        public string expires_in;

        [DataMember]
        public string refresh_expires_in;

        [DataMember]
        public string refresh_token;

        [DataMember]
        public string token_type;

        [DataMember]
        public string not_before_policy;

        [DataMember]
        public string session_state;

        [DataMember]
        public string scope;

    }

}
