# Gestão de validade da credencial de acesso

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

### Variáveis que devem ser configuradas

Abaixo seguem os passos principais:
* Ao iniciar, a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token** (token de renovação).

O access token armazenado em memória válido e monitorado é disponibilizado ao clicar no botão "Visualizar Token".

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a emissão dos tokens.

| Variável | Descrição | 
| ------ | ------ | 
| client_id | id-aplicacao (obtido em https://api-assinatura.bry.com.br/aplicacoes) | WebForm1.aspx.cs
| client_secret | api-key (obtido em https://api-assinatura.bry.com.br/aplicacoes) | WebForm1.aspx.cs 
| checkInterval | Intervalo entre checagens da duração do token, em segundos | WebForm1.aspx.cs

### Tech

O exemplo utiliza as seguintes tecnologias e bibliotecas CSharp abaixo:
* [System.IdentityModel.Tokens.Jwt] - Biblioteca Csharp padrão para emissão, encoding e decoding de tokens JWT;
* [Microsoft.IdentityModel.Tokens] - Biblioteca padrão para representação dos tokens JWT;
* [FluentScheduler] - Agendador de tarefas. É utilizado para automatizar a emissão dos tokens JWT no exemplo.

### Observações

Os atributos de data e hora codificados nos tokens JWT (access token e refresh token), seguem o fuso horário UTC-0.
Lembrando que o Brasil possui 4 fusos horários:
* UTC-2 - Atol das Rocas e Fernando de Noronha.
* UTC-3 **(oficial)** - Estados não citados nos demais itens.
* UTC-4 - Amazonas, Mato Grosso, Mato Grosso do Sul, Rondônia e Roraima.
* UTC-5 - Acre e alguns municípios do Amazonas.

### Uso

    - Passo 1: Execute o Build da aplicação utilizando o Microsoft Visual Studio.
    - Passo 2: Execute o exemplo.

 [System.IdentityModel.Tokens.Jwt]: <https://docs.microsoft.com/en-us/dotnet/api/system.identitymodel.tokens.jwt?view=azure-dotnet>
 [Microsoft.IdentityModel.Tokens]: <https://docs.microsoft.com/pt-br/dotnet/api/system.identitymodel.tokens?view=dotnet-plat-ext-5.0>
 [FluentScheduler]: <https://github.com/fluentscheduler/FluentScheduler>
